//import reusable componets
import { Fragment } from "react";
import About from "./Components/About";
import Form from "./Components/Form";

function App() {
  let listOfForm = [
    {
      name: "binar",
      title: "Biodata",
      content: "untuk kelengkapan administrasi",
      label: "Appy Binar Bootcamp",
    },
    {
      name: "coding bootcamp sebelah",
      title: "Biodata",
      content: "untuk pendaftaran",
      label: "Daftar bootcamp",
    },
  ];

  return (
    <div className="home">
      <header>
        <h1>Learn React</h1>
      </header>
      <main>
        {listOfForm.map((item) => (
          <Fragment>
            <About title={item.title} content={item.content} />
            <Form labelSubmit={item.label} />
          </Fragment>
        ))}
      </main>
    </div>
  );
}

export default App;

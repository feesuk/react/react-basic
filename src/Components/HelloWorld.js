//example of functional components
import logoReact from "../Assets/Images/logo.png";

export default function HelloWorld() {
  return (
    <div>
      <p>Hallo World, I'm Learning React</p>
      <img src={logoReact} alt="logo-react" />
    </div>
  );
}

//contoh functional component

export default function About(props) {
  return (
    <div className="about">
      <h3>Form : {props.title}</h3>
      <div>{props.content}</div>
    </div>
  );
}

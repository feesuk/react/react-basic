import React, { Component } from "react";

export default class Forms extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
    };
  }

  //funtion untuk handle submit
  handleSumbit = (event) => {
    event.preventDefault(); // mencegah agar page tidak relaod sebagaimana default

    alert(`email ${this.state.email} milik ${this.state.name} telah terdaftar`);
  };

  handeChange = (key, value) => {
    switch (key) {
      case "name":
        this.setState({ name: value });
        break;
      case "email":
        this.setState({ email: value });
        break;
      default:
        break;
    }
  };

  render() {
    return (
      <form className="form" onSubmit={this.handleSumbit}>
        <table className="form-wraper">
          <tr>
            <td className="label">Full Name</td>
            <td>
              <input
                onChange={(event) =>
                  this.handeChange("name", event.target.value)
                }
                type="text"
                placeholder="Input Your Full Name"
                className="input"
              />
            </td>
          </tr>
          <tr>
            <td className="label">E-mail</td>
            <td>
              <input
                onChange={(event) =>
                  this.handeChange("email", event.target.value)
                }
                type="email"
                placeholder="Input Your Email"
                className="input"
              />
            </td>
          </tr>
        </table>

        <button type="submit" className="button-submit">
          {this.props.labelSubmit}
        </button>
      </form>
    );
  }
}
